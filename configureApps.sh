#!/usr/bin/env bash
set -e -o pipefail

ALL_POSSIBLE=false
CANDIDATES=( )
CONFIGURE_SCRIPT_FOLDER="$(pwd)/configureApps"
CONFIG_FILE="$(pwd)/configCredentials.cfg"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0") [ARGUMENT] APP..."
        echo "Where APP... are apps to isntall"
        echo "ARGUMENT can be"
        echo "    --config FILE use this config file, default: \"${CONFIG_FILE}\""
        exit
    fi
done

# parse arguments
while [ -n "$1" ]; do
    if [[ "$1" == "--config" ]]; then
        shift
        CONFIG_FILE="$1"
    elif [[ "$1" == "--all" ]]; then
        ALL_POSSIBLE=true
    elif [[ "$1" == "--"* ]]; then
        echo "Unknown argument \"$1\""
        exit 1
    else
        CANDIDATES+=("$1")
    fi
    shift
done

if [ ! -d "$CONFIGURE_SCRIPT_FOLDER" ]; then
  echo "$CONFIGURE_SCRIPT_FOLDER is not found!" && exit 1
fi

# config file
if [ ! -f "$CONFIG_FILE" ]; then
  echo "$CONFIG_FILE is not found!" && exit 1
fi

if [[ "$ALL_POSSIBLE" == true ]]; then
  CANDIDATES="$(ls -1 $CONFIGURE_SCRIPT_FOLDER)"
elif [ "${#CANDIDATES}" == 0 ]; then
  echo "please specify the apps to configure."
fi

for CANDIDATE in ${CANDIDATES[@]}; do
    CANDIDATE_SCRIPT="${CONFIGURE_SCRIPT_FOLDER}/${CANDIDATE}/${CANDIDATE}-configure.sh"
    if [ ! -f "$CANDIDATE_SCRIPT" ]; then
      echo "script for $CANDIDATE is missing. ($CANDIDATE_SCRIPT not found)"
    else
      cd "${CONFIGURE_SCRIPT_FOLDER}/${CANDIDATE}"
      "$CANDIDATE_SCRIPT" "$CONFIG_FILE"
    fi


done
