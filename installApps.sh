#!/usr/bin/env bash
set -e -o pipefail

CANDIDATES=( )
INSTALL_SCRIPT_FOLDER="$(pwd)/installApps"

# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 APPS"
    echo "where APPS are apps to install"
    exit
  fi
done

# check if run as root
if [[ "$(id --user)" != 0 ]]; then
    echo "Script must run as root"
    if [[ -n "$(which sudo)" ]];then
        echo "Try with sudo"
        sudo "$0" "$@"
        exit
    fi
    exit 1
fi

# parse arguments
while [ -n "$1" ]; do
    if [[ "$1" == "--"* ]]; then
        echo "Unknown argument \"$1\""
        exit 1
    else
        CANDIDATES+=("$1")
    fi
    shift
done

if [ ! -d "$INSTALL_SCRIPT_FOLDER" ]; then
  echo "$INSTALL_SCRIPT_FOLDER is not found!" && exit 1
fi

if [ "${#CANDIDATES}" == 0 ]; then
  echo "please specify the apps to install."
fi

for CANDIDATE in "${CANDIDATES[@]}"; do
    INSTALL_SCRIPT="${INSTALL_SCRIPT_FOLDER}/${CANDIDATE}/${CANDIDATE}-install.sh"
    if [ ! -f "$INSTALL_SCRIPT" ]; then
      echo "script for $CANDIDATE is missing. (\"$INSTALL_SCRIPT\" not found)"
    else
      cd "${INSTALL_SCRIPT_FOLDER}/${CANDIDATE}"
      "$INSTALL_SCRIPT"
    fi
done
