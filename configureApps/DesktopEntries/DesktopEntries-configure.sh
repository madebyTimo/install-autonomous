#!/usr/bin/env bash

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="DesktopEntry="
DEPENDENCIES=( )

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $0 [CONFIG]"
        echo "where CONFIG is the path to the config file (configCredentials.cfg)"
        exit
    fi
done

for cmd in ${DEPENDENCIES[@]}; do
    [ -z "$(command -v $cmd)" ] && echo "$cmd is missing!" && exit 1
done

# settings file
if [ -n "$1" ]; then
    SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
    echo "$SETTINGS is not found!" && exit 1
fi

echo "set DesktopEntries"
rm --recursive --force ~/.local/share/applications/custom
mkdir -p ~/.local/share/applications/custom
mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^"$SETTINGS_PREFIX"//")
for SETTING in "${SETTINGS_ARR[@]}"; do
    NAME="${SETTING%%' - '*}"
    ROW="${SETTING#*' - '}"
    FILE="$HOME/.local/share/applications/custom/${NAME}.desktop"
    if [[ ! -f "$FILE" ]]; then
        echo "create Desktop Entry \"$NAME\""
        echo "#!/usr/bin/env xdg-open" > "$FILE"
        echo "[Desktop Entry]" >> "$FILE"
        echo "Name=$NAME" >> "$FILE"
        chmod +x "$FILE"
    fi
    echo "$ROW" >> "$FILE"
done
