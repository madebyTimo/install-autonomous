#!/usr/bin/env bash
set -e -o pipefail

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="Autostart="

# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 [CONFIG]"
    echo "where CONFIG is the path to the config file (configCredentials.cfg)"
    exit
  fi
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo "Configure Autostart."

mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
if [[ -n "${SETTINGS_ARR[*]}" ]]; then
    mkdir --parents ~/.config/autostart
    for SETTING in "${SETTINGS_ARR[@]}"; do
        if [[ "$SETTING" == *.desktop ]]; then
            cp "$SETTING" ~/.config/autostart
        else
            DESKTOP_FILE="${HOME}/.config/autostart/${SETTING//'/'/-}.desktop"
            cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=$(basename "$SETTING")
Exec=$SETTING
Terminal=false
Type=Application
EOF
            chmod +x "$DESKTOP_FILE"
        fi
    done
fi
