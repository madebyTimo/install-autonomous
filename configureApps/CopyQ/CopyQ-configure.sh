#!/usr/bin/env bash
set -e -o pipefail

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="CopyQ="

# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 [CONFIG]"
    echo "where CONFIG is the path to the config file (configCredentials.cfg)"
    exit
  fi
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo "Configure CopyQ."

mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
if [[ -n "${SETTINGS_ARR[*]}" ]]; then
    if [[ -f ~/.config/copyq/copyq.conf ]]; then
        REPLACE=true
    else
        REPLACE=false
        RESULT="[Options]"
    fi
    for SETTING in "${SETTINGS_ARR[@]}"; do
        if [[ "$REPLACE" == true ]]; then
            sed --in-place "s|^${SETTING%% *}=.*\$|${SETTING/ /=}|g" ~/.config/copyq/copyq.conf
        else
            RESULT+="
${SETTING/ /=}"
        fi
    done
    if [[ "$REPLACE" != true ]]; then
        mkdir --parents ~/.config/copyq
        echo "$RESULT" > ~/.config/copyq/copyq.conf
    fi
fi
