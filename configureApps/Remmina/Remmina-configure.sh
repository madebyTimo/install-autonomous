#!/usr/bin/env bash
set -e -o pipefail

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="Remmina="

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo "Configure Remmina."

mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
if [[ -n "${SETTINGS_ARR[*]}" ]]; then
    mkdir --parents ~/.local/share/remmina
    rm --force ~/.local/share/remmina/*.remmina
    for SETTING in "${SETTINGS_ARR[@]}"; do
        CONNECTION_NAME="${SETTING%%" - "*}"
        SETTING="${SETTING#*" - "}"
        FILE_NAME="${HOME}/.local/share/remmina/${CONNECTION_NAME// /-}.remmina"
        echo "[remmina]" > "$FILE_NAME"
        echo "name=$CONNECTION_NAME" >> "$FILE_NAME"
        echo "${SETTING// /$'\n'}" >> "$FILE_NAME"
    done
fi
