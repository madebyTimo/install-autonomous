#!/usr/bin/env bash

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="Gtk="

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail

# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 [CONFIG]"
    echo "where CONFIG is the path to the config file (configCredentials.cfg)"
    exit
  fi
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo set Gtk
mkdir --parents ~/.config/gtk-3.0
echo > ~/.config/gtk-3.0/bookmarks
mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
for SETTING in "${SETTINGS_ARR[@]}"; do
  if [[ $SETTING == "bookmark "* ]]; then
    echo "${SETTING#"bookmark "}" >> ~/.config/gtk-3.0/bookmarks
  fi
done
