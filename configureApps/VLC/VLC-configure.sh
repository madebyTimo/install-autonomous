#!/usr/bin/env bash

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="VLC="

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail

# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 [CONFIG]"
    echo "where CONFIG is the path to the config file (configCredentials.cfg)"
    exit
  fi
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo "Configure VLC."

mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
MAIN_WINDOW=()
for SETTING in "${SETTINGS_ARR[@]}"; do
  if [[ $SETTING == "MainWindow "* ]]; then
    MAIN_WINDOW+=("${SETTING#"MainWindow "}")
  fi
done

mkdir -p ~/.config/vlc
echo "[MainWindow]" > ~/.config/vlc/vlc-qt-interface.conf
for OPTION in "${MAIN_WINDOW[@]}"; do
  echo "$OPTION" >> ~/.config/vlc/vlc-qt-interface.conf
done
