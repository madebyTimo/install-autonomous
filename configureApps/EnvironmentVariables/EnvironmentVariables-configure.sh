#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=()
SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="EnvironmentVariable="


# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 CONFIG"
    echo "where CONFIG is the path to the config file (configCredentials.cfg)"
    exit
  fi
done

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo "Set environment variables."
grep "^$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX/export /" \
    > "${HOME}/.env"
for PROFILE in "${HOME}/.bashrc" "${HOME}/.xsessionrc"; do
    if ! [[ -f "$PROFILE" ]] || ! grep --quiet "^source \"\${HOME}/.env\"\$" "$PROFILE"; then
        echo "source \"\${HOME}/.env\"" >> "$PROFILE"
    fi
done
