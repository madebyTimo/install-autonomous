#!/usr/bin/env bash

SETTINGS="$(pwd)/credentials.txt"
DEPENDENCIES=(firefox)

for cmd in "${DEPENDENCIES[@]}"; do
  [ -z "$(command -v $cmd)" ] && echo "$cmd is missing!" && exit 1
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo set firefox as default
xdg-settings set default-web-browser firefox.desktop

FIREFOX_PROFILE="$(grep --max-count=1 "Default=.*\.default*" "$HOME/.mozilla/firefox/profiles.ini" | cut -d"=" -f2)"

echo create user.js
