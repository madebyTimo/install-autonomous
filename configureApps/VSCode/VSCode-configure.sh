#!/usr/bin/env bash

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="VSCode="

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail

# settings file
if [ -n "$1" ]; then
    SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
    echo "$SETTINGS is not found!" && exit 1
fi

echo configure VSCode

CODE_SETTINGS="{"
mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
for SETTING in "${SETTINGS_ARR[@]}"; do
    if [[ $SETTING == "install-extension "* ]]; then
        code --install-extension "${SETTING#"install-extension "}" --force
    elif [[ $SETTING == "setting "* ]]; then
        CODE_SETTINGS+="${SETTING#"setting "},"
    fi
done
CODE_SETTINGS+="}"
mkdir -p ~/.config/Code/User
echo "$CODE_SETTINGS" > ~/.config/Code/User/settings.json
