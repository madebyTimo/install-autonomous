#!/usr/bin/env bash
set -e -o pipefail

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="MoneroGui="

# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 [CONFIG]"
    echo "where CONFIG is the path to the config file (configCredentials.cfg)"
    exit
  fi
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo "Configure MoneroGUI."

mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
GENERAL=()
for SETTING in "${SETTINGS_ARR[@]}"; do
  if [[ $SETTING == "General "* ]]; then
    GENERAL+=("${SETTING#"General "}")
  fi
done

mkdir -p ~/.config/monero-project
echo "[General]" > ~/.config/monero-project/monero-core.conf
for OPTION in "${GENERAL[@]}"; do
  echo "$OPTION" >> ~/.config/monero-project/monero-core.conf
done
