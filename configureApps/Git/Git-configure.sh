#!/usr/bin/env bash
set -e -o pipefail

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="Git="


# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 [CONFIG]"
    echo "where CONFIG is the path to the config file (configCredentials.cfg)"
    exit
  fi
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo "Configure Git."

mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
CREDENTIALS_CLEANED=false
for SETTING in "${SETTINGS_ARR[@]}"; do
    if [[ "$SETTING" == "config --global "* ]]; then
        eval git "$SETTING"
    elif [[ "$SETTING" == "credentials "* ]]; then
        if [[ "$CREDENTIALS_CLEANED" != true ]]; then
            true > ~/.git-credentials
            CREDENTIALS_CLEANED=true
        fi
        echo "${SETTING#"credentials "}" >> ~/.git-credentials
    fi
done
