#!/usr/bin/env bash

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="Linphone="

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail

# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 [CONFIG]"
    echo "where CONFIG is the path to the config file (configCredentials.cfg)"
    exit
  fi
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo "Set Linphone"
CREDENTIALS_HA1=""
USERNAME=""
REALM=""
SIP_ADDRESS=""
SIP_SERVER_ADDRESS=""
mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
for SETTING in "${SETTINGS_ARR[@]}"; do
    if [[ "$SETTING" == credentials-ha1=* ]]; then
        CREDENTIALS_HA1="${SETTING#credentials-ha1=}"
    elif [[ "$SETTING" == username=* ]]; then
        USERNAME="${SETTING#username=}"
    elif [[ "$SETTING" == realm=* ]]; then
        REALM="${SETTING#realm=}"
    elif [[ "$SETTING" == sip-address=* ]]; then
        SIP_ADDRESS="${SETTING#sip-address=}"
    elif [[ "$SETTING" == sip-server-address=* ]]; then
        SIP_SERVER_ADDRESS="${SETTING#sip-server-address=}"
    elif [[ "$SETTING" == stun-server=* ]]; then
        STUN_SERVER="${SETTING#stun-server=}"
    else
        echo "not supported: \"$SETTING\""
    fi
done

mkdir --parents ~/.config/linphone
cat > ~/.config/linphone/linphonerc << EOF
[sip]
default_proxy=0

[auth_info_0]

[auth_info_1]
username=$USERNAME
userid=$USERNAME
ha1=$CREDENTIALS_HA1
realm=$REALM

[nat_policy_1]
ref=TDuaEX02di5SukJ
stun_server=$STUN_SERVER
protocols=stun,ice

[proxy_0]
nat_policy_ref=TDuaEX02di5SukJ
reg_proxy=$SIP_SERVER_ADDRESS
reg_identity=$SIP_ADDRESS

[sound]
playback_dev_id=ALSA Unknown: default
ringer_dev_id=ALSA Unknown: default
capture_dev_id=ALSA Unknown: default
EOF
