#!/usr/bin/env bash

SETTINGS="$(pwd)/configCredentials.cfg"
SETTINGS_PREFIX="Locale="
DEPENDENCIES=(localectl)

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail

# help message
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
    echo "usage: $0 [CONFIG]"
    echo "where CONFIG is the path to the config file (configCredentials.cfg)"
    exit
  fi
done

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# settings file
if [ -n "$1" ]; then
  SETTINGS="$1"
fi
if [ ! -f "$SETTINGS" ]; then
  echo "$SETTINGS is not found!" && exit 1
fi

echo set Locale
mapfile -t SETTINGS_ARR < <(grep "$SETTINGS_PREFIX" "$SETTINGS" | sed --expression "s/^$SETTINGS_PREFIX//")
for SETTING in "${SETTINGS_ARR[@]}"; do
  if [[ "$SETTING" == "system "* ]]; then
    SETTING="${SETTING#"system "}"
    eval localectl set-locale "$SETTING"
  elif [[ "$SETTING" == "user "* ]]; then
    SETTING="${SETTING#'user '}"
    if [[ -f ~/.xsessionrc ]]; then
        sed --in-place "/^export LANG=/d" ~/.xsessionrc
    fi
    echo "export $SETTING" >> ~/.xsessionrc
  else
    echo "not supported: \"$SETTING\""
  fi
done
