#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl python3)
TEMPDIRECTORY="$(pwd)/.temp-TorBrowser-install"
DOWNLOADS_JSON_URL="https://aus1.torproject.org/torbrowser/update_3/release/downloads.json"


# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done


echo "Configure Tor Browser."

mkdir "$TEMPDIRECTORY"
cd "$TEMPDIRECTORY"
DOWNLOADS_JSON_FILE="./downloads.json"
curl --silent --output "$DOWNLOADS_JSON_FILE" "$DOWNLOADS_JSON_URL"
JSON_PATH="downloads.linux-x86_64.ALL.binary"

function traverseJson(){
  JSON_FILE="$1" JSON_PATH="$2" python3 -c "
import json, os
jsonPath = os.environ['JSON_PATH']
filePath = os.environ['JSON_FILE']
file = open(filePath)
data = json.load(file)
arrayPath = jsonPath.split('.')
for path in arrayPath:
    data = data[path]
print(data)
"
}

PACKAGE_URL="$(traverseJson "$DOWNLOADS_JSON_FILE" "$JSON_PATH")"
curl --silent --output TorBrowser.tar.xz "$PACKAGE_URL"
tar --extract --file TorBrowser.tar.xz
rm TorBrowser.tar.xz
mv tor-browser* TorBrowser
rm -f -r ~/.TorBrowser
mv TorBrowser ~/.TorBrowser
cd -
rm -r "$TEMPDIRECTORY"

~/.TorBrowser/Browser/start-tor-browser --register-app
