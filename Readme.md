# Install-Autonomous

This repository aims to install and configure apps autonomous without user interaction. 

The scripts are written for debian with a cinnamon desktop. For other system configurations some scripts need adaptions. 

## Configure Apps

These scrrips need to be executed as user.

### Firefox

### Git

### GSettings

### Gtk

### NetworkManager

you can use `nmcli connection show $CONNECTION_NAME` to retrieve the information about a configured network. To see the secrets add `--show-secrets`.

### Tor Browser

## InstallApps

These scripts need too be executed as root.

### Basics

Install some basic applications which are useful on mostly every system.

### Discord

### DisplayLink

Install the DisplayLink driver.

### Docker

### Firefox

### FreeCAD

### htop

### IntellijIdea

### Java

### MintThemes

### npm

### Ramdisk

Automatically mount a Ramdisk at startup and backup the content on shutdown.

### Ultimaker Cura

### VLC

### VSCode
