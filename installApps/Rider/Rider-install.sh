#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl gzip tar)
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Rider."

VERSION="$(curl --silent --location "https://www.jetbrains.com/rider/download/" | \
    sed --silent 's|^.*"What.s New", "version": "\([^"]*\)".*$|\1|p' | \
    head --lines 1)"

if [[ -n "$(which rider)" ]] && (rider --version | grep "$VERSION" > /dev/null); then
    echo "Version \"${VERSION}\" is already installed."
    exit
fi

echo "Install version \"${VERSION}\"."
mkdir "$WORKING_DIR"
cd "$WORKING_DIR"
curl --silent --location --output "rider.tar.gz" \
    "https://download-cdn.jetbrains.com/rider/JetBrains.Rider-${VERSION}.tar.gz"
tar --extract --gzip --file ./rider.tar.gz
rm rider.tar.gz
mv "JetBrains Rider"* rider
chmod --recursive a+r rider
rm -r --force /opt/rider
mv rider /opt

ln --symbolic --force /opt/rider/bin/rider.sh /usr/local/bin/rider
DESKTOP_FILE="/usr/share/applications/rider.desktop"
cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Rider
Exec=/usr/local/bin/rider
Icon=intellij
Categories=TextEditor;Development;IDE;
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"
