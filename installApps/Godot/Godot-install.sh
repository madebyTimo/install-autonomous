#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl unzip)
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT


echo "Install Godot."

RELEASE_VERSION="$(curl --silent --location \
    "https://api.github.com/repos/godotengine/godot/releases/latest" \
    | sed --silent 's|^\s*"tag_name": "\(.*\)".*$|\1|p' \
    | head --lines 1)"
PACKAGE_URL="https://github.com/godotengine/godot/releases/download/${RELEASE_VERSION}\
/godot_v${RELEASE_VERSION}_linux.x86_64.zip"

if [[ -n "$(which godot)" ]] && [[ "$(godot --version)" == "${RELEASE_VERSION/-/.}"* ]]; then
    echo "Version \"${RELEASE_VERSION}\" is already installed."
    exit
fi

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

curl --silent --location --output godot.zip "$PACKAGE_URL"
unzip -p godot.zip \
    > /usr/local/bin/godot
rm godot.zip
chmod +x /usr/local/bin/godot

DESKTOP_FILE="/usr/share/applications/Godot.desktop"
cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Godot
Exec=godot
Icon=godot
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"
