#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl gzip tar)
TEMPDIRECTORY="$(pwd)/.temp-Intellij-install"

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Intellij Idea."

VERSION="$(curl --silent --location "https://www.jetbrains.com/idea/download/" | \
    sed --silent 's|^.*"What.s New", "version": "\([^"]*\)".*$|\1|p' | \
    head --lines 1)"

if [[ -n "$(which intellijIdea)" ]] && (intellijIdea --version | grep "$VERSION" > /dev/null); then
    echo "Version \"${VERSION}\" is already installed."
    exit
fi

echo "Install version \"${VERSION}\"."
mkdir "$TEMPDIRECTORY"
cd "$TEMPDIRECTORY"
curl --silent --location --output "IntellijIdea.tar.gz" \
    "https://download.jetbrains.com/idea/ideaIC-${VERSION}.tar.gz"
tar --extract --gzip --file ./IntellijIdea.tar.gz
rm IntellijIdea.tar.gz
mv idea-IC* IntellijIdea
chmod --recursive a+r IntellijIdea
rm -r --force /opt/IntellijIdea
mv IntellijIdea /opt
rm -r "$TEMPDIRECTORY"

ln --symbolic --force /opt/IntellijIdea/bin/idea.sh /usr/local/bin/intellijIdea
DESKTOP_FILE="/usr/share/applications/IntellijIdea.desktop"
cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Intellij Idea
Exec=/usr/local/bin/intellijIdea
Icon=intellij-idea
Categories=TextEditor;Development;IDE;
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"
