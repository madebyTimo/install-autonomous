#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl gzip python3 tar)
WORKING_DIR="/tmp/.temp-$(basename "$0")"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Portfolio Performance."

RELEASE_VERSION="$(curl --silent --location \
    "https://api.github.com/repos/buchen/portfolio/releases/latest" \
    | sed --silent 's|^\s*"tag_name": "\(.*\)".*$|\1|p' \
    | head --lines 1)"

if [[ -f /opt/portfolio-performance/Version.txt ]] \
    && [[ "$(cat /opt/portfolio-performance/Version.txt)" == "$RELEASE_VERSION" ]]; then
    echo "Version \"${RELEASE_VERSION}\" is already installed."
    exit
fi

if [[ -e "$WORKING_DIR" ]]; then
    echo "\"${WORKING_DIR}\" exists already. Removing in 10 seconds."
    sleep 10
    rm -f -r "$WORKING_DIR"
fi
mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

PACKAGE_URL="https://github.com/portfolio-performance/portfolio/releases/download/\
${RELEASE_VERSION}/PortfolioPerformance-${RELEASE_VERSION}-linux.gtk.x86_64.tar.gz"
curl --silent --location "$PACKAGE_URL" \
    | tar --extract --gzip --warning "no-unknown-keyword"
mv portfolio portfolio-performance
chmod --recursive a+r portfolio-performance
rm -r --force /opt/portfolio-performance
mv portfolio-performance /opt
rm -r "$WORKING_DIR"

ln --symbolic --force /opt/portfolio-performance/PortfolioPerformance \
    /usr/local/bin/portfolio-performance
DESKTOP_FILE="/usr/share/applications/portfolio-performance.desktop"
cat >"$DESKTOP_FILE" <<EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Portfolio Performance
Exec=/usr/local/bin/portfolio-performance
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"

echo "$RELEASE_VERSION" >/opt/portfolio-performance/Version.txt
