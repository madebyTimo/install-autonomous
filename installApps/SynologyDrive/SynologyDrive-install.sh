#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl dpkg)
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

echo "Install Synology Drive."

RELEASE_VERSION="$(curl --location --fail --silent "https://archive.synology.com/download/Utility/SynologyDriveClient" \
    | sed --silent 's|^.*<a href="/download/Utility/SynologyDriveClient/\([^"]*\)".*$|\1|p' \
    | head --lines 1)"

INSTALLED_VERSION="$(dpkg-query --show --showformat '${Version}\n' synology-drive 2&> /dev/null|| true)"

if [[ "$RELEASE_VERSION" == "$INSTALLED_VERSION" ]]; then
    echo "Version \"${RELEASE_VERSION}\" is already installed."
    exit
fi

PACKAGE_URL="https://global.synologydownload.com/download/Utility/SynologyDriveClient/\
${RELEASE_VERSION}/Ubuntu/Installer/synology-drive-client-${RELEASE_VERSION#*-}.x86_64.deb"

mkdir "$WORKING_DIR"
curl --silent --location --output "$WORKING_DIR/synology-drive.deb" "$PACKAGE_URL"
apt install -y -qq "$WORKING_DIR/synology-drive.deb"
