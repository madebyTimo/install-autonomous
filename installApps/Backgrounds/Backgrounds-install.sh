#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl)
RELEASES_JSON_URL="https://api.github.com/repos/mbT-Infrastructure/backgrounds/releases/latest"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install backgrounds."

RELEASE_VERSION="$(curl --silent --location "$RELEASES_JSON_URL" \
    | grep --max-count 1 "\"tag_name\": " \
    | sed "s|^\s*\".*\": \"\(.*\)\".*\$|\1|")"
PACKAGE_URL="https://github.com/mbT-Infrastructure/backgrounds/releases/download/${RELEASE_VERSION}\
/backgrounds-${RELEASE_VERSION}.tar.zst"
rm -f -r /usr/local/share/backgrounds/mbt
mkdir --parents /usr/local/share/backgrounds/mbt

curl --silent --location "$PACKAGE_URL" | \
    tar --directory /usr/local/share/backgrounds/mbt --extract --strip-components 1 --zstd
