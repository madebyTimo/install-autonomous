#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl gpg)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done


echo install Google Chrome

curl --silent https://dl.google.com/linux/linux_signing_key.pub \
    | gpg --yes --dearmor --output /usr/share/keyrings/google-chrome.gpg
echo "deb [signed-by=/usr/share/keyrings/google-chrome.gpg] http://dl.google.com/linux/chrome/deb \
stable main" \
    > /etc/apt/sources.list.d/google-chrome.list

apt update -qq
apt install -y -qq google-chrome-stable
