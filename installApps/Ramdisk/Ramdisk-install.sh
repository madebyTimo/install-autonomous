#!/usr/bin/env bash
set -e -o pipefail

service="$(pwd)/ramdisk.service"
DEPENDENCIES=(systemctl)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

if [ ! -f "$service" ]; then
  echo "$service is not found!" && exit 1
fi

echo install Ramdisk

cp "$service" /etc/systemd/system/ramdisk.service
mkdir --parents /media/Ramdisk/
chmod a+rwx /media/Ramdisk/

systemctl daemon-reload
systemctl enable ramdisk.service
systemctl start ramdisk.service
