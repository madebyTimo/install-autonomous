#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl)
PACKAGE_URL="https://www.synaptics.com/sites/default/files/Ubuntu/pool/stable/main/all/\
synaptics-repository-keyring.deb"
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

echo "Install DisplayLink."

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

curl --silent --location --output "displaylink-repo.deb" "$PACKAGE_URL"

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
KERNEL_RELEASE="$(uname -r)"
apt install -y -qq --mark-auto "linux-headers-$KERNEL_RELEASE"
apt install -y -qq "./displaylink-repo.deb" "linux-headers-${KERNEL_RELEASE#*-}"
apt update -qq

# DisplayLink installation creates a file there and fails if folder does not exist
mkdir --parents /lib/systemd/system-sleep

apt install -qq --yes displaylink-driver
apt purge -y -qq synaptics-repository-keyring


if [[ ! -f /lib/systemd/system-sleep/displaylink.sh ]]; then
    echo "File \"/lib/systemd/system-sleep/displaylink.sh\" not created!"
    echo "Please check neccessity of the creation from the folder!"
fi
