#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl)
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

echo "Install Verus Desktop."

VERSION="$(curl --silent --location \
    "https://api.github.com/repos/VerusCoin/Verus-Desktop/releases/latest" \
    | sed --silent 's|^\s*"tag_name": "\(.*\)".*$|\1|p' \
    | head --lines 1)"

if [[ -n "$(which verus-desktop)" ]] && [[ -f /opt/verus-desktop/Version.txt ]] \
        && [[ "$(cat /opt/verus-desktop/Version.txt)" == "$VERSION" ]]; then
    echo "Version \"${VERSION}\" is already installed."
    exit
fi

PACKAGE_URL="https://github.com/VerusCoin/Verus-Desktop/releases/download/${VERSION}\
/Verus-Desktop-Linux-${VERSION}-x86_64.tgz"

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

curl --silent --location "$PACKAGE_URL" | \
    tar --extract --gzip
chmod +x Verus-Desktop-*.AppImage
mv Verus-Desktop-*.AppImage /usr/local/bin/verus-desktop
mkdir --parents /opt/verus-desktop
echo "$VERSION" > /opt/verus-desktop/Version.txt

DESKTOP_FILE="/usr/share/applications/VerusDesktop.desktop"
cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Verus Desktop
Exec=verus-desktop
Icon=bitcoin
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"
