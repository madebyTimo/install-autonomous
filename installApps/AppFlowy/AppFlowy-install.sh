#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl)
RELEASES_JSON_URL="https://api.github.com/repos/AppFlowy-IO/appflowy/releases/latest"
WORKDIR="${PWD}/.temp-$(basename "$0")"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install AppFlowy."

mkdir "$WORKDIR"
cd "$WORKDIR"
RELEASES_JSON_FILE="./downloads.json"
curl --silent --location --output "$RELEASES_JSON_FILE" "$RELEASES_JSON_URL"
JSON_PATH="assets.2.browser_download_url"

function traverseJson(){
  JSON_FILE="$1" JSON_PATH="$2" python3 -c "
import json, os
jsonPath = os.environ['JSON_PATH']
filePath = os.environ['JSON_FILE']
file = open(filePath)
data = json.load(file)
arrayPath = jsonPath.split('.')
for path in arrayPath:
    if path.isdigit():
      path = int(path)
    data = data[path]
print(data)
"
}

PACKAGE_URL="$(traverseJson "$RELEASES_JSON_FILE" "$JSON_PATH")"
curl --silent --location --output "appflowy.deb" "$PACKAGE_URL"
apt install -y -qq "$WORKDIR/appflowy.deb"
rm -f -r "$WORKDIR"
