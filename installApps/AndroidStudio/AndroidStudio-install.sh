#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl sha256sum)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Android Studio."

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
apt install -y -qq libc6 libncurses5 libstdc++6 lib32z1 libbz2-1.0


VERSION_CHECKSUM="$(curl --fail --silent --location "https://developer.android.com/studio" \
    | sed --silent --null-data \
    "s|^.*>android-studio-\([0-9\.]*\)-linux.tar.gz</button>[^<>]*</td>[^<>]*<td>[^<>]*</td>[^<>]*\
<td>\([^<>]*\)</td>.*\$|\1 \2|p")"
VERSION="${VERSION_CHECKSUM%' '*}"
CHECKSUM="${VERSION_CHECKSUM#*' '}"

if [[ -n "$(which android-studio)" ]] \
        && android-studio --version | grep "${VERSION%"."*}" > /dev/null; then
    echo "Version \"${VERSION}\" is already installed."
    exit
fi

PACKAGE_URL="https://redirector.gvt1.com/edgedl/android/studio/ide-zips/${VERSION}/\
android-studio-${VERSION}-linux.tar.gz"

rm -f -r /opt/android-studio

curl --silent --location "$PACKAGE_URL" \
    | tee >(tar --directory /opt --extract --gzip) \
    | sha256sum --check --status <(echo "$CHECKSUM  -")

ln --symbolic --force /opt/android-studio/bin/studio.sh /usr/local/bin/android-studio
DESKTOP_FILE="/usr/share/applications/android-studio.desktop"
cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Android Studio
Exec=/usr/local/bin/android-studio
Icon=android-studio
Categories=TextEditor;Development;IDE;
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"
