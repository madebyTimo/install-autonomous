#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which $CMD)" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install hardware tools."

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
apt install -y -qq smbios-utils

if (lspci | grep --quiet "VGA compatible controller:"); then
    apt install -y -qq nvtop
fi

if (lspci | grep --quiet "VGA compatible controller: Intel"); then
    apt install -y -qq intel-gpu-tools
fi
