#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl gpg)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done


echo "Install VSCode."

curl --silent --location https://packages.microsoft.com/keys/microsoft.asc | \
    gpg --yes --dearmor --output /usr/share/keyrings/packages.microsoft.gpg
echo "deb [signed-by=/usr/share/keyrings/packages.microsoft.gpg] \
https://packages.microsoft.com/repos/code stable main" \
    > /etc/apt/sources.list.d/vscode.list

apt update -qq
apt install -y -qq code
