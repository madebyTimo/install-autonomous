#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl python3)
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done


echo "Install Ultimaker Cura."

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

VERSION="$(curl --silent --location \
    "https://api.github.com/repos/Ultimaker/Cura/releases/latest" \
    | sed --silent 's|^\s*"tag_name": "\(.*\)".*$|\1|p' \
    | head --lines 1)"

if [[ -n "$(which ultimaker-cura)" ]] && [[ -f /opt/ultimaker-cura/Version.txt ]] \
        && [[ "$(cat /opt/ultimaker-cura/Version.txt)" == "$VERSION" ]]; then
    echo "Version \"${VERSION}\" is already installed."
    exit
fi

PACKAGE_URL="https://github.com/Ultimaker/Cura/releases/download/${VERSION}/\
UltiMaker-Cura-${VERSION}-linux-X64.AppImage"

curl --silent --location --output UltimakerCura.AppImage "$PACKAGE_URL"
chmod a+rx UltimakerCura.AppImage
mkdir --parents /opt/ultimaker-cura
echo "$VERSION" > /opt/ultimaker-cura/Version.txt
mv UltimakerCura.AppImage /usr/local/bin/ultimaker-cura

DESKTOP_FILE="/usr/share/applications/ultimaker-cura.desktop"
cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Ultimaker Cura
Exec=ultimaker-cura
Icon=applications-development
Categories=Engineering;
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"
