#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt)
SCRIPT_DIR="$(dirname "$(realpath "$0")")"


# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Podman."

RELEASE_CODENAME="$(lsb_release --codename --short)"
if [[ "$RELEASE_CODENAME" == "trixie" ]]; then
    RELEASE_CODENAME="bookworm"
fi

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
apt install -y -qq apt-transport-https ca-certificates
curl --silent "https://download.docker.com/linux/$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")/gpg" | \
    gpg --yes --dearmor --output /usr/share/keyrings/docker.gpg
echo "deb [signed-by=/usr/share/keyrings/docker.gpg] https://download.docker.com/linux/$(lsb_release --id --short | tr "[:upper:]" "[:lower:]") $RELEASE_CODENAME stable" > /etc/apt/sources.list.d/docker.list
apt update -qq
apt install -y -qq docker-compose-plugin podman podman-compose

curl --silent --location --output /etc/containers/registries.conf \
    https://raw.githubusercontent.com/mbT-Infrastructure/template-config-files/main/debian/podman/registries.conf
curl --silent --location --output /etc/containers/storage.conf \
    https://raw.githubusercontent.com/mbT-Infrastructure/template-config-files/main/debian/podman/storage.conf

if [[ ! -f /usr/bin/docker ]] \
    || grep --silent "# Docker --> Podman wrapper script" /usr/bin/docker; then
    echo "Place docker wrapper script."
    cp "${SCRIPT_DIR}/docker" /usr/bin/docker
fi
