#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Firmware."

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
apt install -qq --yes fwupd
fwupdmgr refresh --force
fwupdmgr update --assume-yes --no-reboot-check || [[ $? == 2 ]]


MISSING_FIRMWARE_FILES="$(dmesg | grep "firmware: failed to load" || true)"

echo "This may add \"non-free-firmware\" and \"non-free\" packages."

aptUpdate() {
    TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
        "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
    if [[ "$PACKAGES_ADDED" == true ]] || [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
        apt update -qq
    fi
}

addNonFreeFirmware() {
    PACKAGES_ADDED=false
    if (ls /etc/apt/sources.list.d/stable-main.list > /dev/null 2>&1) \
        && [[ ! -f /etc/apt/sources.list.d/stable-non-free-firmware.list ]]; then
        echo "Add apt source \"stable-non-free-firmware\"."
        curl --silent --location --output /etc/apt/sources.list.d/stable-non-free-firmware.list \
            "https://raw.githubusercontent.com/mbT-Infrastructure/template-config-files/main/\
debian/apt/sources-stable-non-free-firmware.list"
        PACKAGES_ADDED=true
    fi

    if (ls /etc/apt/sources.list.d/testing-main.list > /dev/null 2>&1) \
        && [[ ! -f /etc/apt/sources.list.d/testing-non-free-firmware.list ]]; then
        echo "Add apt source \"testing-non-free-firmware\"."
        curl --silent --location --output /etc/apt/sources.list.d/testing-non-free-firmware.list \
            "https://raw.githubusercontent.com/mbT-Infrastructure/template-config-files/main/\
debian/apt/sources-testing-non-free-firmware.list"
        PACKAGES_ADDED=true
    fi
    aptUpdate
}

addNonFree() {
    PACKAGES_ADDED=false
    if (ls /etc/apt/sources.list.d/stable-main.list > /dev/null 2>&1) \
        && [[ ! -f /etc/apt/sources.list.d/stable-non-free.list ]]; then
        echo "Add apt source \"stable-non-free\"."
        curl --silent --location --output /etc/apt/sources.list.d/stable-non-free.list \
            "https://raw.githubusercontent.com/mbT-Infrastructure/template-config-files/main/\
debian/apt/sources-stable-non-free.list"
        PACKAGES_ADDED=true
    fi

    if (ls /etc/apt/sources.list.d/testing-main.list > /dev/null 2>&1) \
        && [[ ! -f /etc/apt/sources.list.d/testing-non-free.list ]]; then
        echo "Add apt source \"testing-non-free\"."
        curl --silent --location --output /etc/apt/sources.list.d/testing-non-free.list \
            "https://raw.githubusercontent.com/mbT-Infrastructure/template-config-files/main/\
debian/apt/sources-testing-non-free.list"
        PACKAGES_ADDED=true
    fi
    aptUpdate
}

addContrib() {
    PACKAGES_ADDED=false
    if (ls /etc/apt/sources.list.d/stable-main.list > /dev/null 2>&1) \
        && [[ ! -f /etc/apt/sources.list.d/stable-contrib.list ]]; then
        echo "Add apt source \"stable-contrib\"."
        curl --silent --location --output /etc/apt/sources.list.d/stable-contrib.list \
            "https://raw.githubusercontent.com/mbT-Infrastructure/template-config-files/main/\
debian/apt/sources-stable-contrib.list"
        PACKAGES_ADDED=true
    fi

    if (ls /etc/apt/sources.list.d/testing-main.list > /dev/null 2>&1) \
        && [[ ! -f /etc/apt/sources.list.d/testing-contrib.list ]]; then
        echo "Add apt source \"testing-contrib\"."
        curl --silent --location --output /etc/apt/sources.list.d/testing-contrib.list \
            "https://raw.githubusercontent.com/mbT-Infrastructure/template-config-files/main/\
debian/apt/sources-testing-contrib.list"
        PACKAGES_ADDED=true
    fi
    aptUpdate
}

if (grep --quiet "ath10k/" <<<"$MISSING_FIRMWARE_FILES"); then
    addNonFreeFirmware
    apt install -y -qq firmware-atheros
fi

if (grep --quiet "i915/adlp" <<<"$MISSING_FIRMWARE_FILES"); then
    addNonFreeFirmware
    apt install -y -qq firmware-misc-nonfree
fi

if (grep --quiet "intel/sof" <<<"$MISSING_FIRMWARE_FILES"); then
    addNonFreeFirmware
    apt install -y -qq firmware-sof-signed
fi

if (grep --quiet "iwlwifi" <<<"$MISSING_FIRMWARE_FILES"); then
    addNonFreeFirmware
    apt install -y -qq firmware-iwlwifi
fi

if (grep --quiet "nvidia/" <<<"$MISSING_FIRMWARE_FILES"); then
    addNonFreeFirmware
    addNonFree
    addContrib
    KERNEL_RELEASE="$(uname -r)"
    apt install -y -qq --mark-auto "linux-headers-$KERNEL_RELEASE"
    apt install -y -qq firmware-misc-nonfree "linux-headers-${KERNEL_RELEASE##*-}" nvidia-driver
fi

if (grep --quiet "rtl_nic/" <<<"$MISSING_FIRMWARE_FILES") \
    || (grep --quiet "rtw88/" <<<"$MISSING_FIRMWARE_FILES"); then
    addNonFreeFirmware
    apt install -y -qq firmware-realtek
fi

if (lspci | grep --quiet "VGA compatible controller.*\[AMD/ATI\]"); then
    addNonFreeFirmware
    apt install -y -qq firmware-amd-graphics
fi
