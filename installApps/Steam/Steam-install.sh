#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl gpg)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done


echo "Install Steam."

curl --silent --location --output /usr/share/keyrings/steam.gpg \
    "https://repo.steampowered.com/steam/archive/stable/steam.gpg"
echo "deb [signed-by=/usr/share/keyrings/steam.gpg] \
https://repo.steampowered.com/steam/ stable steam" \
    > /etc/apt/sources.list.d/steam.list

dpkg --add-architecture i386
apt update -qq
apt install -y -qq libgl1-mesa-dri:amd64 libgl1-mesa-dri:i386 libgl1-mesa-glx:amd64 \
    libgl1-mesa-glx:i386 steam-launcher
