#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt gpg)

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Bruno."

rm -f /usr/share/keyrings/bruno.gpg
curl --silent \
'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xe598552e09c999055fefa7159fa6017ecabe0266' \
    | gpg --yes --dearmor --output /usr/share/keyrings/bruno.gpg
rm -f /usr/share/keyrings/bruno.gpg~
echo "deb [signed-by=/usr/share/keyrings/bruno.gpg] http://debian.usebruno.com/ bruno stable" \
    > /etc/apt/sources.list.d/bruno.list

apt update -qq
apt install -y -qq bruno
