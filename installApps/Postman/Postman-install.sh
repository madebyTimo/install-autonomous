#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl)
PACKAGE_URL="https://dl.pstmn.io/download/latest/linux_64"
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

echo "Install Postman."

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"
curl --silent --location --output "postman.tar.gz" "$PACKAGE_URL"
tar --extract --file postman.tar.gz
rm -r --force /opt/Postman
mv Postman /opt

ln --symbolic --force /opt/Postman/Postman /usr/local/bin/postman
DESKTOP_FILE="/usr/share/applications/Postman.desktop"
cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Postman
Exec=/usr/local/bin/postman
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"
