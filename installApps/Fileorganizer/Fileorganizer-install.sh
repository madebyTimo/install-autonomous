#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl)
PACKAGE_URL="https://nas.madebytimo.de:5011/index.php/s/9jyekR8go7KQKqy/download"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Fileorganizer."

curl --silent --location --output /usr/local/bin/fileorganizer "$PACKAGE_URL"
chmod +x /usr/local/bin/fileorganizer
