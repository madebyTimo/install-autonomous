#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl gpg)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which $CMD)" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "install Node.js"

NODE_VERSION="$(curl --silent --location https://deb.nodesource.com/setup_lts.x | grep '^NODE_VERSION=' | sed "s|NODE_VERSION=\"\(.*\)\"|\1|g")"

curl --silent --location https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | \
    gpg --yes --dearmor --output /usr/share/keyrings/nodesource.gpg
echo "deb [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_VERSION nodistro main" > \
    /etc/apt/sources.list.d/nodesource.list

apt update -qq
apt install -y -qq nodejs
