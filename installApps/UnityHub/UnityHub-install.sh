#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl gpg)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done


echo "Install Unity Hub."

curl --silent --location https://hub.unity3d.com/linux/keys/public | \
    gpg --yes --dearmor --output /usr/share/keyrings/unity-technologies.gpg
echo "deb [signed-by=/usr/share/keyrings/unity-technologies.gpg] https://hub.unity3d.com/linux/repos/deb stable main" > \
    /etc/apt/sources.list.d/unityhub.list

apt update -qq
apt install -y -qq unityhub
