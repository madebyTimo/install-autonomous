#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl)
PACKAGE_URL="https://downloads.getmonero.org/gui/linux64"
SIGNATURE_PUBLIC_KEY_URLS=( \
    "https://raw.githubusercontent.com/monero-project/monero/master/utils/gpg_keys/binaryfate.asc" )
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

echo "Install Monero GUI."

PACKAGE_URL="$(curl --head --silent https://downloads.getmonero.org/gui/linux64 \
    | sed --silent 's|^Location: \(.*\)\r$|\1|p')"
VERSION="$(sed --quiet 's|^.*monero-gui-linux-.*-\(.*\).tar..*$|\1|p' <<< "$PACKAGE_URL")"

if [[ -x /opt/monero-gui/monerod ]] \
        && [[ "$(/opt/monero-gui/monerod --version | sed 's|^.*(\(v.*\)-release).*$|\1|')" \
            == "$VERSION" ]]; then
    echo "Version \"${VERSION}\" is already installed."
    exit
fi

if [[ -e "$WORKING_DIR" ]]; then
    echo "\"${WORKING_DIR}\" exists already. Removing in 10 seconds."
    sleep 10
    rm -f -r "$WORKING_DIR"
fi
mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

for SIGNATURE_PUBLIC_KEY_URL in "${SIGNATURE_PUBLIC_KEY_URLS[@]}"; do
    curl --silent --location "$SIGNATURE_PUBLIC_KEY_URL" \
        | gpg --yes --dearmor >> signature-public-keys.gpg
done
curl --silent --location --remote-name "$PACKAGE_URL"
curl --silent --location --output hashes.txt "https://www.getmonero.org/downloads/hashes.txt"

gpg --keyring ./signature-public-keys.gpg --no-default-keyring --quiet \
        --verify hashes.txt &> /dev/null \
    || echo "Signature validation failed!"
sed --in-place --quiet 's|\(monero-gui-linux-x64-.*.tar.bz2\)|\1|p' hashes.txt
sha256sum --check --quiet hashes.txt
rm hashes.txt signature-public-keys.gpg

tar --extract --file monero-gui-*.tar.bz2
rm -fr monero-gui-*.tar.bz2 /opt/monero-gui
mv monero-gui-* /opt/monero-gui
ln --symbolic --force /opt/monero-gui/monero-wallet-gui /usr/local/bin/monero-gui

curl --silent --location --output /opt/monero-gui/icon.png \
    "https://www.getmonero.org/press-kit/symbols/monero-symbol-480.png"

rm -f -r "$WORKING_DIR"

mkdir --parents /usr/local/share/applications
DESKTOP_FILE="/usr/local/share/applications/monero-gui.desktop"
cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Monero GUI
Exec=monero-gui
Icon=/opt/monero-gui/icon.png
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"
