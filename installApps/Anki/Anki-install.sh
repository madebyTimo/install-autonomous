#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl python3 tar zstd)
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

echo "Install Anki."

VERSION="$(curl --silent --location \
    "https://api.github.com/repos/ankitects/anki/releases/latest" \
    | sed --silent 's|^\s*"tag_name": "\(.*\)".*$|\1|p' \
    | head --lines 1)"

if [[ -n "$(which anki)" ]] \
        && [[ "$(anki --version | sed --silent 's|^Anki \([^ ]*\)$|\1|p' | tail --lines 1)" \
        == "$VERSION" ]]; then
    echo "Version \"${VERSION}\" is already installed."
    exit
fi

PACKAGE_URL="https://github.com/ankitects/anki/releases/download/${VERSION}/\
anki-${VERSION}-linux-qt6.tar.zst"

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
apt install -y -qq libxcb-cursor0

if [[ -f /usr/local/share/anki/uninstall.sh ]]; then
    /usr/local/share/anki/uninstall.sh
fi

curl --silent --location "$PACKAGE_URL" | \
    tar --extract --zstd
mv anki* Anki
cd Anki
./install.sh > /dev/null
