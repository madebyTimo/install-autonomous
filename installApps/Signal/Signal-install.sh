#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl gpg)

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Signal."

curl --silent https://updates.signal.org/desktop/apt/keys.asc \
    | gpg --yes --dearmor --output /usr/share/keyrings/signal.gpg
echo "deb [signed-by=/usr/share/keyrings/signal.gpg]" \
    "https://updates.signal.org/desktop/apt xenial main" \
    > /etc/apt/sources.list.d/signal.list

apt update -qq
apt install -y -qq signal-desktop
