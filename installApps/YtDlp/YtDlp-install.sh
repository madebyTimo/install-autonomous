#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl)
PACKAGE_URL="https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install yt-dlp."

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
apt install -y -qq python3-brotli python3-certifi python3-mutagen python3-pycryptodome \
    python3-websockets

curl --silent --location --output /usr/bin/yt-dlp "$PACKAGE_URL"
chmod +x /usr/bin/yt-dlp
