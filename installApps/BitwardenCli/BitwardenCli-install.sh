#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl unzip)
PACKAGE_URL="https://vault.bitwarden.com/download/?app=cli&platform=linux"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Bitwarden CLI."

curl --silent --location --output .temp-bitwarden.zip "$PACKAGE_URL"
unzip -p .temp-bitwarden.zip \
    > /usr/local/bin/bw
rm .temp-bitwarden.zip
chmod a+x /usr/local/bin/bw
