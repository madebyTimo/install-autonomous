#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Firefox ESR."

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
apt install -y -qq firefox-esr firefox-esr-l10n-de

mkdir --parents /etc/firefox/policies
curl --silent --location --output "/etc/firefox/policies/policies.json" \
    "https://github.com/mbT-Infrastructure/template-config-files/raw/main/debian/firefox/\
policies.json"
