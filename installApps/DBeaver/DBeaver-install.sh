#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl)
PACKAGE_URL="https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb"
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

echo "Install DBeaver."

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"
curl --silent --location --output dbeaver.deb "$PACKAGE_URL"
apt install -y -qq ./dbeaver.deb
