#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl gpg)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Docker."

RELEASE_CODENAME="$(lsb_release --codename --short)"
if [[ "$RELEASE_CODENAME" == "trixie" ]]; then
    RELEASE_CODENAME="bookworm"
fi

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
apt install -y -qq apt-transport-https ca-certificates
curl --silent "https://download.docker.com/linux/$(\
    lsb_release --id --short | tr "[:upper:]" "[:lower:]")/gpg" \
    | gpg --yes --dearmor --output /usr/share/keyrings/docker.gpg
echo "deb [signed-by=/usr/share/keyrings/docker.gpg]" \
    "https://download.docker.com/linux/$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")" \
    "$RELEASE_CODENAME stable" \
    > /etc/apt/sources.list.d/docker.list

NVIDIA_GPU=false
if lspci | grep --quiet "VGA compatible controller: NVIDIA" && lsmod | grep --silent nvidia; then
    NVIDIA_GPU=true
    ARCHITECTURE="$(dpkg --print-architecture)"
    curl --silent "https://nvidia.github.io/libnvidia-container/gpgkey" \
        | gpg --yes --dearmor --output /usr/share/keyrings/libnvidia-container.gpg
    echo "deb [signed-by=/usr/share/keyrings/libnvidia-container.gpg]" \
        "https://nvidia.github.io/libnvidia-container/stable/deb/${ARCHITECTURE} /" \
        > /etc/apt/sources.list.d/libnvidia-container.list
fi

apt update -qq
apt install -y -qq docker-ce docker-compose-plugin

if [[ "$NVIDIA_GPU" == true ]]; then
    apt install -y -qq nvidia-container-toolkit
    nvidia-ctk runtime configure --runtime=docker
    systemctl restart docker
fi
