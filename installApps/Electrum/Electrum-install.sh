#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl)
SIGNATURE_PUBLIC_KEY_URLS=( \
    "https://raw.githubusercontent.com/spesmilo/electrum/master/pubkeys/ThomasV.asc" \
    "https://raw.githubusercontent.com/spesmilo/electrum/master/pubkeys/sombernight_releasekey.asc" \
    "https://raw.githubusercontent.com/spesmilo/electrum/master/pubkeys/Emzy.asc" )
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

echo "Install Electrum."

RELEASE_VERSION="$(curl --silent --location "https://electrum.org" \
    | sed --silent "s|^.*Latest release: Electrum-\([0-9\.]*\)<.*\$|\1|p" \
    | head --lines 1)"
PACKAGE_URL="https://download.electrum.org/${RELEASE_VERSION}/\
electrum-${RELEASE_VERSION}-x86_64.AppImage"

if [[ -n "$(which electrum)" ]] \
        && [[ "$(electrum --offline --version)" == "$RELEASE_VERSION" ]]; then
    echo "Version \"${RELEASE_VERSION}\" is already installed."
    exit
fi

if [[ -e "$WORKING_DIR" ]]; then
    echo "\"${WORKING_DIR}\" exists already. Removing in 10 seconds."
    sleep 10
    rm -f -r "$WORKING_DIR"
fi
mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

for SIGNATURE_PUBLIC_KEY_URL in "${SIGNATURE_PUBLIC_KEY_URLS[@]}"; do
    curl --silent --location "$SIGNATURE_PUBLIC_KEY_URL" \
        | gpg --yes --dearmor >> signature-public-keys.gpg
done
curl --silent --location --output electrum "$PACKAGE_URL"
curl --silent --location --output electrum.asc "${PACKAGE_URL}.asc"

gpg --keyring ./signature-public-keys.gpg --no-default-keyring --quiet \
        --verify electrum.asc electrum &> /dev/null \
    || echo "Signature validation failed!"
chmod +x electrum
mv electrum /usr/local/bin/electrum

rm -f -r "$WORKING_DIR"

DESKTOP_FILE="/usr/share/applications/Electrum.desktop"
cat > "$DESKTOP_FILE" << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Electrum
Exec=electrum
Icon=bitcoin
Terminal=false
Type=Application
EOF
chmod +x "$DESKTOP_FILE"
