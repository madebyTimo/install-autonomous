#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl gpg)

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done


echo "install Dotnet"

RELEASE_CODENAME="$(lsb_release --codename --short)"
RELEASE_NUMBER="$(lsb_release --release --short)"

if [[ "$RELEASE_CODENAME" == "trixie" ]]; then
    RELEASE_CODENAME="bookworm"
    RELEASE_NUMBER=12
fi

curl --silent --location https://packages.microsoft.com/keys/microsoft.asc | \
    gpg --yes --dearmor --output /usr/share/keyrings/packages.microsoft.gpg
echo "deb [signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/debian/${RELEASE_NUMBER}/prod $RELEASE_CODENAME main" > \
    /etc/apt/sources.list.d/dotnet.list

apt update -qq
apt install -y -qq dotnet-sdk-7.0
