#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl)
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

echo "install Python"

TIME_SINCE_LAST_UPDATE="$(( "$(date "+%s")" - \
    "$(date "+%s" --reference /var/cache/apt/pkgcache.bin || echo 0)" ))"
if [[ "$TIME_SINCE_LAST_UPDATE" -gt 3600 ]]; then
    apt update -qq
fi
apt install -y -qq libffi-dev libsqlite3-dev libssl-dev pkg-config python3-dev python3-pip \
    python3-venv zlib1g-dev

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

RELEASE_VERSION="$(curl --silent --compressed --location \
    "https://www.python.org/downloads/source/" \
    | sed --silent "s|^.*Latest Python 3 Release - Python \(3[0-9\.]*\)<.*\$|\1|p" \
    | head --lines 1)"

if [[ -n "$(which "python${RELEASE_VERSION%.*}")" ]] \
        && [[ "$("python${RELEASE_VERSION%.*}" --version)" == "Python $RELEASE_VERSION" ]]; then
    echo "Version \"${RELEASE_VERSION}\" is already installed."
    exit
fi

curl --silent --location \
    "https://www.python.org/ftp/python/${RELEASE_VERSION}/Python-${RELEASE_VERSION}.tgz" \
    | tar --extract --gzip

cd Python-*
./configure --silent
make altinstall --jobs "$(nproc)" --silent
ln --symbolic --force "/usr/local/bin/python${RELEASE_VERSION%.*}" /usr/local/bin/python3-latest
ln --symbolic --force "/usr/local/bin/pip${RELEASE_VERSION%.*}" /usr/local/bin/pip3-latest
