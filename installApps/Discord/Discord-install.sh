#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt curl)
PACKAGE_URL="https://discord.com/api/download?platform=linux&format=deb"
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

echo "Install Discord."

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"
curl --silent --location --output discord.deb "$PACKAGE_URL"

apt install -y -qq ./discord.deb
