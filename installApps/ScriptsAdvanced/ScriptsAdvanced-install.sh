#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl)
PACKAGE_URL="https://gitlab.com/madebyTimo/scripts-advanced/-/archive/main/\
scripts-advanced-main.tar.gz"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install advanced scripts."

curl --silent --location "$PACKAGE_URL" | \
    tar --directory /usr/local/bin/ --extract --gzip --strip-components 2 --wildcards \
        '*/scripts/*.sh'
