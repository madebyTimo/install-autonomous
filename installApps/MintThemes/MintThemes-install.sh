#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(apt cinnamon curl gpg)
MINT_VERSION="elsie"

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which $CMD)" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo install Mint-Themes

curl --silent 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x302f0738f465c1535761f965a6616109451bbbf2' | gpg --yes --dearmor --output /usr/share/keyrings/packages.linuxmint.gpg
echo "deb [signed-by=/usr/share/keyrings/packages.linuxmint.gpg] http://packages.linuxmint.com $MINT_VERSION main" > /etc/apt/sources.list.d/linuxmint.list

apt update -qq
apt install -y -qq mint-themes
