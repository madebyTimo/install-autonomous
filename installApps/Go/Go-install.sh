#!/usr/bin/env bash
set -e -o pipefail

DEPENDENCIES=(curl)
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

function cleanup {
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

echo "Install Go."

RELEASE_VERSION="$(curl --silent --location "https://go.dev/dl/" \
    | sed --silent "s|^.*go\(.*\)\.linux-amd64.tar.gz.*$|\1|p" \
    | head --lines 1)"

if [[ -n "$(which go)" ]] && (go version | grep "go$RELEASE_VERSION" > /dev/null); then
    echo "Version \"${RELEASE_VERSION}\" is already installed."
    exit
fi

case "$(uname --machine)" in
    "x86_64") ARCHITECTURE="amd64" ;;
    "aarch64") ARCHITECTURE="arm64" ;;
    *) echo "Unknown architecture."; exit 1 ;;
esac

if [[ -e "$WORKING_DIR" ]]; then
    echo "\"${WORKING_DIR}\" exists already. Removing in 10 seconds."
    sleep 10
    rm -f -r "$WORKING_DIR"
fi
mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

PACKAGE_URL="https://go.dev/dl/go${RELEASE_VERSION}.linux-${ARCHITECTURE}.tar.gz"
rm -f -r /opt/go

curl --silent --location "$PACKAGE_URL" | \
    tar --directory /opt --extract --gzip

ln --symbolic --force /opt/go/bin/* /usr/local/bin
